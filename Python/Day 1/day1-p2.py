'''
During the second Go / No Go poll, the Elf in charge of the Rocket Equation Double-Checker stops the launch sequence. 
Apparently, you forgot to include additional fuel for the fuel you just added.

Fuel itself requires fuel just like a module - 
take its mass, divide by three, round down, and subtract 2. 
However, that fuel also requires fuel, and that fuel requires fuel, and so on. 
Any mass that would require negative fuel should instead be treated as if it requires zero fuel; the remaining mass, if any, is instead handled by wishing really hard, which has no mass and is outside the scope of this calculation.

So, for each module mass, calculate its fuel and add it to the total. Then, treat the fuel amount you just calculated as the input mass and repeat the process, continuing until a fuel requirement is zero or negative.

For Example: The fuel required by a module of mass 100756 and its fuel is: 33583 + 11192 + 3728 + 1240 + 411 + 135 + 43 + 12 + 2 = 50346
'''

import os
from math import floor

module_masses = [line.rstrip('\n') for line in open(os.environ['INPUT'])]
total_fuel_req = 0

def calc_fuel_req(mass):
    fuel_calc = 0
    calc = floor(mass/3) - 2
    if calc > 0:
        fuel_calc += calc
        fuel_calc += calc_fuel_req(fuel_calc)
    return fuel_calc

for mass in module_masses:
    total_fuel_req += calc_fuel_req(int(mass))

print(total_fuel_req)