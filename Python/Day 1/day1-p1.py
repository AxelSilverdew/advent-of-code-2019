'''
The Elves quickly load you into a spacecraft and prepare to launch.

At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. 
They haven't determined the amount of fuel required yet.

Fuel required to launch a given module is based on its mass. 
Specifically, to find the fuel required for a module, take its mass, 
divide by three, round down, and subtract 2.

For Example: For a mass of 100756, the fuel required is 33583.
'''

import os
from math import floor

module_masses = [line.rstrip('\n') for line in open(os.environ['INPUT'])]
total_fuel_req = 0

def calc_fuel_req(mass):
    global total_fuel_req
    return floor(mass/3) - 2

for mass in module_masses:
    total_fuel_req += calc_fuel_req(int(mass))

print(total_fuel_req)